﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class Grass : MonoBehaviour
{
    public Material mat;
    private MaterialPropertyBlock prop;
    private Renderer rend;
    public List<Transform> plants;
    private int currentPlants;
    public Vector4[] test;
    [Range(0, 50)]
    public float radius;

    //TEST
    public float timer;
    private float resetTimer;
    public Transform[] testObject;
    private int currentObject = 0;

    void Start()
    {
        resetTimer = timer;
    }

    void Update()
    {
        radius += Time.deltaTime * 0.5f;
        mat.SetFloat("_Radius", radius);
        mat.SetInt("_Points_Length", 100);
        //foreach (var item in plants)
        //{
        //    mat.SetVectorArray("_Points", item.transform.position);
        //    radius = 5;
        //    mat.SetFloat("_Proporties", radius);
        //}

        timer -= Time.deltaTime;
        if (timer < 0 && currentObject < testObject.Length)
        {
            plants.Add(testObject[currentObject]);
            currentObject++;
            timer = resetTimer;
            InsertArray();
        }
    }

    public void debug()
    {
        //Debug.Log(mat.GetVector("_Center"));

    }

    public void InsertArray()
    {
        Vector4[] points = new Vector4[100];

        for (int i = 0; i < plants.Count; i++)
        {
            points[i].x = plants[i].transform.position.x;
            points[i].y = plants[i].transform.position.y;
            points[i].z = plants[i].transform.position.z;
            //points[i].w = plants[i].GetComponent<BasePlant>().radius;
            points[i].w = radius;
        }

        mat.SetVectorArray("_Points", points);
        Vector4[] debug = mat.GetVectorArray("_Points");
        foreach (var item in debug)
        {
            Debug.Log(item);
        }

    }
}
