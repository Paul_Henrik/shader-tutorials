﻿Shader "Paul/GrasArray" {
	Properties {
		_MainTex ("Splat", 2D) = "white" {}
		[NoScaleOffset] _TextureGras ("Grass Texture", 2D) = "white" {}
		[NoScaleOffset] _TextureGround ("Ground Texture", 2D) = "white" {}
		[NoScaleOffset] _myBumpGround ("Ground Normal", 2D) = "bump" {}
        _mySlider ("Bump Amount", Range(0,10)) = 1

	    _Center("Center", Vector) = (0,0,0,0)
	    _Radius("Radius", Float) = 0.5
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Lambert fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _TextureGras, _TextureGround, _myBumpGround;

		float3 _Center;
		float _Radius;
		half _mySlider;

		struct Input {
			float2 uv_MainTex;
			float2 uv_Splat;
			float2 uv_myBumpGround;
			float3 worldPos;
		};

		int _Points_Length = 0;
		uniform float3 _Points [100]; //x,y,z. The position
		uniform float _Proporties [100];

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutput o) {
			// Albedo comes from a texture tinted by color
			o.Albedo = tex2D(_TextureGround, IN.uv_MainTex);
			o.Normal = UnpackNormal(tex2D(_myBumpGround, IN.uv_myBumpGround * 10));
            o.Normal *= float3(_mySlider,_mySlider,1);
			
			for(int i = 0; i < _Points_Length; i++)
			{
				float d = distance(_Points[i], IN.worldPos);
				float4 splat = tex2D(_MainTex, IN.uv_MainTex);
			if (d < _Radius)
			{
				o.Albedo = tex2D(_TextureGras, IN.uv_MainTex) * splat.g + tex2D(_TextureGround, IN.uv_MainTex) * (1 - splat.g);
			}
				
			// else

			}

		}
		ENDCG
	} 
	FallBack "Diffuse"
}
