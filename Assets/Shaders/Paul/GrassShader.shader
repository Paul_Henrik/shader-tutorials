﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Paul/GrassShader"
{
		Properties{
			_MainTex ("SplatMap", 2D) = "white" {}
			_Center ("Center", Vector) = (0,0,0,0)
			_Radius ("Radius", float) = 2
			[NoScaleOffset] _Texture1 ("Texture 1", 2D) = "white" {}
			[NoScaleOffset] _Texture2 ("Texture 2", 2D) = "white" {}
		}
		SubShader{
			
			Pass {
				
				CGPROGRAM	//Stuck with a oddities like this due to backward compatibility

				#pragma vertex MyVertexProgram
				#pragma fragment MyFragmentProgram

				#include "UnityCG.cginc"

				sampler2D _MainTex;
				float4 _MainTex_ST;
				float3 _Center;
				float _Radius;

				sampler2D _Texture1, _Texture2;

				struct Interpolators {
					float4 position : SV_POSITION;
					float2 uv : TEXCOORD0;
					float2 uvSplat : TEXCOORD1;
					float3 worldPos : POSITION2;
				};

				struct VertexData {
					float4 position : POSITION;
					float3 uv : TEXCOORD0;
				};

				Interpolators MyVertexProgram (VertexData v){
					Interpolators i;
					i.position = UnityObjectToClipPos(v.position);
					i.uv = TRANSFORM_TEX(v.uv, _MainTex); //UnityCG macro for "v.uv * _MainTex_ST.xy + _MainTex_ST.zw"
					i.uvSplat = v.uv;
					i.worldPos = i.position.xyw;
					i.worldPos.y *= _ProjectionParams.x;
					return i;
				}

				float4 MyFragmentProgram (Interpolators i) : SV_TARGET {
					float2 screenUV = (i.worldPos.xy / i.worldPos.z) * 0.5f + 0.5f;
					
					float d = distance(_Center.xz, i.worldPos);
					float4 splat = tex2D(_MainTex, i.uv);
					if (d > _Radius)
					{
						return tex2D(_Texture1, i.uv);
					}
					else
						return tex2D(_Texture1, i.uv) * splat.g + tex2D(_Texture2, i.uv) * (1 - splat.g);

				}
			// float d = distance(_Center, IN.worldPos);
			// if (d > _Radius && d < _Radius + _RadiusWidth)
			// 	o.Albedo = _RadiusColor;
			// else
			// 	o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;

				ENDCG
			}
		}
}
