﻿Shader "Paul/GrasV2" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Splat", 2D) = "white" {}
		[NoScaleOffset] _TextureGras ("GrasTexture", 2D) = "white" {}
		[NoScaleOffset] _TextureGround ("GroundTexture", 2D) = "white" {}

	    _Center("Center", Vector) = (0,0,0,0)
	    _Radius("Radius", Float) = 0.5
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _TextureGras, _TextureGround;
		float3 _Center;
		float _Radius;

		struct Input {
			float2 uv_MainTex;
			float2 uv_Splat;
			float3 worldPos;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			float d = distance(_Center, IN.worldPos);
			float4 splat = tex2D(_MainTex, IN.uv_MainTex);
			
			if (d > _Radius)
				o.Albedo = tex2D(_TextureGround, IN.uv_MainTex);
			else
				o.Albedo = tex2D(_TextureGras, IN.uv_MainTex) * splat.g + tex2D(_TextureGround, IN.uv_MainTex) * (1 - splat.g);
		}
		ENDCG
	}
	FallBack "Diffuse"
}
