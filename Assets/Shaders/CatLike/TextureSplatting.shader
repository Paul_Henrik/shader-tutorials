﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Catlike/TextureSplatting"
{
		Properties{
			_MainTex ("SplatMap", 2D) = "white" {}
			[NoScaleOffset] _Texture1 ("Texture 1", 2D) = "white" {}
			[NoScaleOffset] _Texture2 ("Texture 2", 2D) = "white" {}
		}
		SubShader{
			
			Pass {
				
				CGPROGRAM	//Stuck with a oddities like this due to backward compatibility

				#pragma vertex MyVertexProgram
				#pragma fragment MyFragmentProgram

				#include "UnityCG.cginc"

				sampler2D _MainTex;
				float4 _MainTex_ST;

				sampler2D _Texture1, _Texture2;

				struct Interpolators {
					float4 position : SV_POSITION;
					float2 uv : TEXCOORD0;
					float2 uvSplat : TEXCOORD1;
				};

				struct VertexData {
					float4 position : POSITION;
					float2 uv : TEXCOORD0;
				};

				Interpolators MyVertexProgram (VertexData v){
					Interpolators i;
					i.position = UnityObjectToClipPos(v.position);
					i.uv = TRANSFORM_TEX(v.uv, _MainTex); //UnityCG macro for "v.uv * _MainTex_ST.xy + _MainTex_ST.zw"
					i.uvSplat = v.uv;
					return i;
				}

				float4 MyFragmentProgram (Interpolators i) : SV_TARGET {
					float4 splat = tex2D(_MainTex, i.uvSplat);
					return tex2D(_Texture1, i.uv) *splat.g + tex2D(_Texture2, i.uv) * (1 - splat.g);
				}

				ENDCG
			}
		}
}
