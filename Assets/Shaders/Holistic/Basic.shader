﻿Shader "Holistic/BasicLambert"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
	}
		SubShader
	{
		Tags { "Queue" = "Geometry" }
		LOD 100

			CGPROGRAM
		#pragma surface surf Lambert

		float4 _Color;

		struct Input {
			float2 uv_MainTex;
		};

		void surf(Input IN, inout SurfaceOutput o) {
			o.Albedo = _Color.rgb;
		}

		
		ENDCG
		}
	
	FallBack "Diffuse"
}
