﻿Shader "Custom/BumpedEnvironment" {
	Properties {
		_myDiffuse ("Diffuse texture", 2D) = "white" {}
		_myBump ("Bump texture", 2D) = "bump" {}
		_mySlider ("Bump Amount", Range(0,10)) = 1
		_myBrightness ("Brightness", Range(0,10)) = 1
		_myCube ("Cube map", CUBE) = "white" {}
	}
	SubShader {

		CGPROGRAM
		#pragma surface surf Lambert

		sampler2D _myDiffuse;
		sampler2D _myBump;
		half _mySlider;
		half _myBrightness;
		samplerCUBE _myCube;

		struct Input {
			float2 uv_myDiffuse;
			float2 uv_myBump;
			float3 worldRefl; INTERNAL_DATA	//Cubemap needs 3d data.
		};

		void surf (Input IN, inout SurfaceOutput o) {
			o.Albedo = tex2D(_myDiffuse, IN.uv_myDiffuse).rgb;
			o.Normal = UnpackNormal(tex2D(_myBump, IN.uv_myBump)) * _myBrightness;
			o.Normal *= float3(_mySlider,_mySlider, 0);
			o.Emission = texCUBE (_myCube, WorldReflectionVector (IN, o.Normal)).rgb;	//WorldReflectionVector is an internal function
		}
		ENDCG
	}
	FallBack "Diffuse"
}
