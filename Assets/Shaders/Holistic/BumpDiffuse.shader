﻿Shader "Custom/BumpDiffuse" {
	Properties {
		_myDiffuse ("Diffuse texture", 2D) = "white" {}
		_myBump ("Bump texture", 2D) = "bump" {}
		_mySlider ("Bump Amount", Range(0,10)) = 1
		_myBrightness ("Brightness", Range(0,10)) = 1
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Lambert

		sampler2D _myDiffuse;
		sampler2D _myBump;
		half _mySlider;
		half _myBrightness;

		struct Input {
			float2 uv_myDiffuse;
			float2 uv_myBump;
		};

		void surf (Input IN, inout SurfaceOutput o) {
			o.Albedo = tex2D(_myDiffuse, IN.uv_myDiffuse).rgb;
			o.Normal = UnpackNormal(tex2D(_myBump, IN.uv_myBump)) * _myBrightness;
			o.Normal *= float3(_mySlider,_mySlider, 1);
		}
		ENDCG
	}
	FallBack "Diffuse"
}
